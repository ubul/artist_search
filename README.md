artist_search
=============

Create virtualenv:
```
virtualenv env
```
Activate it:
```
env/Scripts/activate
```
or
```
source env/bin/activate
```

Install package in dev mode:
```
python setup.py develop
```

Run tests
```
nosetests
```

Run server
```
python -m artist_search.server
```