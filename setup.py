import os

from setuptools import setup, find_packages
from pip.req import parse_requirements

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

parsed_requirements = parse_requirements(os.path.join(
    here, 'requirements', 'development.txt'))

requires = [str(requirement.req) for requirement in parsed_requirements]

setup(
    name='artist_search',
    version='0.0.1',
    description='Artist search service',
    classifiers=[
      "Programming Language :: Python"
    ],
    author='Zoltan Kozma',
    author_email='kozmaz87@gmail.com',
    url='http://',
    keywords='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    test_suite="nose.collector",
    command_packages=[
      'distutils.command',
    ]
)

