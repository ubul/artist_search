
from flask import Flask, request, jsonify
from flask.json import JSONEncoder
from store import ArtistStore
from artist import Artist


class ExtendedJSONEncoder(JSONEncoder):
    """
    Let's add support for the magic __json__ function
    """
    def default(self, o):
        try:
            json_data = o.__json__()
            return json_data
        except:
            pass
        return JSONEncoder.default(self, o)

app = Flask("Amazing artist search service")

app.json_encoder = ExtendedJSONEncoder

@app.route('/artists', methods=['POST'])
def put_artist():
    json_data = request.get_json(force=True)
    new_id = json_data.get('uuid')
    new_age = json_data.get('age')
    new_artist = Artist(new_id, new_age)
    artist_store.put(new_artist)
    return jsonify({
        'success': True,
        'artist': new_artist
    })

@app.route('/artists', methods=['GET'])
def get_artists():

    result = jsonify({
        'artists': list(artist_store.list())
    })
    return result

@app.route('/artists/<int:from_age>/<int:to_age>', methods=['GET'])
def get_artists_range(from_age, to_age):

    result = jsonify({
        'artists': list(artist_store.query_around_median(from_age, to_age))
    })

    return result

if __name__ == '__main__':
    artist_store = ArtistStore()
    app.run()