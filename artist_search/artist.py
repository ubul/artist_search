

class Artist(object):

    __slots__ = [
        '_uuid',
        '_age'
    ]

    def __init__(self, id_val, age):
        self.uuid = id_val
        self.age = age

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, value):
        # TODO: Validation
        self._age = value

    @property
    def uuid(self):
        return self._uuid

    @uuid.setter
    def uuid(self, value):
        # TODO: Validation
        self._uuid = value

    def __repr__(self):
        return "<{} ({}: {})>".format(
            self.__class__.__name__,
            self.uuid,
            self.age
        )

    def __json__(self):
        return {
            'uuid': self.uuid,
            'age': self.age
        }
