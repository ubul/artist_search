
from unittest import TestCase
from ..artist import Artist
from ..store import ArtistStore


class TestBasicFunctions(TestCase):

    def setUp(self):
        self.store = ArtistStore()

    def test_add(self):
        test_artist = Artist('test_uuid1', 32)
        self.store.put(test_artist)
        self.assertEqual(len(self.store._impl), 1)

    def test_list(self):
        test_artist = Artist('test_uuid1', 32)
        self.store.put(test_artist)
        list_result = list(self.store.list())
        self.assertEqual(len(list_result), 1)
        self.assertEqual(list_result[0].age, 32)
        self.assertEqual(list_result[0].uuid, 'test_uuid1')

    def test_ordering(self):
        self.store.put(Artist('id1', 65))
        self.store.put(Artist('id2', 32))
        self.store.put(Artist('id3', 13))
        self.store.put(Artist('id4', 42))
        # Test for duplicates
        self.store.put(Artist('id5', 13))
        results = self.store.query(13, 45)
        self.assertEqual(len(results), 4)
        expected_order = [13, 13, 32, 42]
        for idx, item in enumerate(results):
            self.assertEqual(item.age, expected_order[idx])

    def test_median_query(self):
        self.store.put(Artist('id1', 65))
        self.store.put(Artist('id2', 32))
        self.store.put(Artist('id3', 13))
        self.store.put(Artist('id4', 42))
        # Test for duplicates
        self.store.put(Artist('id5', 13))
        results = self.store.query_around_median(13, 70)
        expected_order = [32, 42, 13, 13, 65]
        for idx, item in enumerate(results):
            self.assertEqual(item.age, expected_order[idx])

    def tearDown(self):
        self.store = None