
from ..artist import Artist
from unittest import TestCase


class TestBasicOps(TestCase):

    def test_creation(self):
        test_artist = Artist('uuid1', 56)
        self.assertEqual(test_artist.age, 56)
        self.assertEqual(test_artist.uuid, 'uuid1')

    def test_setters(self):
        test_artist = Artist('uuid4', 76)
        test_artist.uuid = 'changed'
        self.assertEqual(test_artist.uuid, 'changed')
        test_artist.age = 20
        self.assertEqual(test_artist.age, 20)


