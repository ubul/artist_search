from .artist import Artist
from sortedcontainers import SortedListWithKey
from Queue import PriorityQueue, Empty


class ArtistStore(object):
    """
    Simple sorted data store to store Artist information.
    It is using a sorted list implementation
    """
    _impl = None

    def __init__(self):
        self._impl = SortedListWithKey(key=lambda x: x.age)

    def put(self, artist):
        self._impl.add(artist)

    def query(self, range_min, range_max):
        art_min = Artist('dummy', range_min)
        art_max = Artist('dummy', range_max)
        idx_min = self._impl.bisect_left(art_min)
        idx_max = self._impl.bisect_right(art_max)
        return self._impl[idx_min:idx_max]

    def query_around_median(self, range_min, range_max):
        median = int(range_max - range_min) / 2
        result_queue = PriorityQueue()
        for item in self.query(range_min, range_max):
            diff = abs(item.age - median)
            result_queue.put((diff, item))

        try:
            while True:
                diff, result_item = result_queue.get_nowait()
                yield result_item

        except Empty:
            raise StopIteration()

    def list(self):
        result = iter(self._impl)
        return result


